package org.example;

public class Costumer {

        private int distance;
        private String name;

        private final int bankCard = 1000;


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getBankCard() {
            return bankCard;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        @Override
        public String toString() {
            return "Costumer{" +
                    "distance=" + distance +
                    ", name='" + name + '\'' +
                    ", bankCard=" + bankCard +
                    '}';
        }


}
